import os
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import ttkbootstrap as ttk
from edit_frame import EditDocument
from qdsb import QDNoteStatusBar


class QDNoteFrameGrid(ttk.Frame):
    def __init__(self, parent):
        ttk.Frame.__init__(self, parent)
        self.parent = parent
        self.parent.title("QDNote")

        self.filetypes = (("Normal text file", "*.txt"), ("all files", "*.*"))
        self.init_dir = os.path.join(os.path.expanduser('~'), 'Desktop')

        # Create Notebook ( for tabs ).
        self.nb = ttk.Notebook(parent)
        self.nb.bind("<Button-2>", self.close_tab)
        self.nb.enable_traversal()
        self.status_bar = QDNoteStatusBar(self.parent)
        self.grid(column=0, row=0)
        self.nb.grid(column=0, row=0, sticky="nsew")
        self.status_bar.grid(column=0, row=1)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=1)

        # Override the X button.
        self.parent.protocol('WM_DELETE_WINDOW', self.exit)
        self._filetypes = (("Normal text file", "*.txt"), ("all files", "*.*"))
        
        # Create Menu Bar
        menubar = tk.Menu(self.parent)
        
        # Create File Menu
        file_menu = tk.Menu(menubar, tearoff=0)
        file_menu.add_command(label="New",
                              accelerator="Ctrl+N",
                              command=self.new_file)
        file_menu.add_command(label="Open",
                              accelerator="Ctrl+O",
                              command=self.open_file)
        file_menu.add_command(label="Save",
                              accelerator="Ctrl+S",
                              command=self.save_file)
        file_menu.add_command(label="Save As...", command=self.save_as)
        file_menu.add_command(label="Close", command=self.close_tab)
        file_menu.add_separator()
        file_menu.add_command(label="Exit", command=self.exit)
        
        # Create Edit Menu
        edit_menu = tk.Menu(menubar, tearoff=0)
        edit_menu.add_command(label="Undo", command=self.undo)
        edit_menu.add_separator()
        edit_menu.add_command(label="Cut", command=self.cut)
        edit_menu.add_command(label="Copy", command=self.copy)
        edit_menu.add_command(label="Paste", command=self.paste)
        edit_menu.add_command(label="Delete", command=self.delete)
        edit_menu.add_command(label="Select All", command=self.select_all)
        
        # Create Format Menu, with a check button for word wrap.
        format_menu = tk.Menu(menubar, tearoff=0)
        self.word_wrap = tk.BooleanVar()
        format_menu.add_checkbutton(label="Word Wrap", onvalue=True, offvalue=False, variable=self.word_wrap, command=self.wrap)
        
        # Attach to Menu Bar
        menubar.add_cascade(label="File", menu=file_menu)
        menubar.add_cascade(label="Edit", menu=edit_menu)
        menubar.add_cascade(label="Format", menu=format_menu)
        self.parent.config(menu=menubar)
        
        # Create right-click menu.
        self.right_click_menu = tk.Menu(self.parent, tearoff=0)
        self.right_click_menu.add_command(label="Undo",
                                          command=self.undo,
                                          accelerator="Ctrl+Z")
        self.right_click_menu.add_separator()
        self.right_click_menu.add_command(label="Cut",
                                          accelerator="Ctrl+X",
                                          command=self.cut)
        self.right_click_menu.add_command(label="Copy",
                                          accelerator="Ctrl+C",
                                          command=self.copy)
        self.right_click_menu.add_command(label="Paste",
                                          accelerator="Ctrl+V",
                                          command=self.paste)
        self.right_click_menu.add_command(label="Delete", command=self.delete)
        self.right_click_menu.add_separator()
        self.right_click_menu.add_command(label="Select All",
                                          accelerator="Ctrl+a",
                                          command=self.select_all)
        
        # Create tab right-click menu
        self.tab_right_click_menu = tk.Menu(self.parent, tearoff=0)
        self.tab_right_click_menu.add_command(label="New Tab", command=self.new_file)
        self.nb.bind('<Button-3>', self.right_click_tab)
        self.bind_all("<Control-n>", self.new_file_hotkey)
        self.bind_all("<Control-o>", self.open_file_hotkey)
        self.bind_all("<Control-s>", self.save_file_hotkey)

        # Create Initial Tab
        first_tab = EditDocument(self.nb)
        self.nb.add(first_tab, text='Untitled')
        self.parent.columnconfigure(1, weight=1)
        self.parent.rowconfigure(1, weight=1)

    def open_file_hotkey(self, _):
        self.open_file()

    def open_file(self):
        current_tab = self.current_tab
        open_file = filedialog.askopenfilename(initialdir=os.chdir(os.path.expanduser("~")),
                                               title="Open file",
                                               filetypes=self._filetypes)
        current_tab.open_file(open_file)
        self.nb.tab(current_tab, text=current_tab.title)

    def save_as(self):
        current_tab = self.current_tab
        save_file = filedialog.asksaveasfilename(initialdir=self.init_dir,
                                                 title="Select file",
                                                 filetypes=self._filetypes,
                                                 defaultextension='.txt')
        if not save_file:
            return
        current_tab.save_file_as(save_file)
        self.nb.tab(current_tab, text=current_tab.title)

    def save_file_hotkey(self, _):
        self.save_file()

    def save_file(self):
        if not self.current_tab.filename:
            self.save_as()
        else:
            self.current_tab.save_file()

    def new_file_hotkey(self, _):
        self.new_file()

    def new_file(self):
        new_tab = EditDocument(self.nb)
        self.nb.add(new_tab, text="Untitled")

    def copy(self):
        self.current_tab.copy_text()

    def delete(self):
        pass

    def cut(self):
        pass

    def wrap(self):
        pass

    def paste(self):
        self.current_tab.paste_text()

    def select_all(self, *args):
        pass

    def undo(self):
        self.current_tab().undo()

    def right_click(self, event):
        self.right_click_menu.post(event.x_root, event.y_root)
        
    def right_click_tab(self, event):
        self.tab_right_click_menu.post(event.x_root, event.y_root)
        
    def close_tab(self, event=None):
        pass

    def exit(self):        
        # Check if any changes have been made.
        if self.save_changes():
            self.parent.destroy()
        else:
            return
               
    def save_changes(self):
        if self.current_tab.modified:
            response = messagebox.askyesnocancel("QDNote", "Do you want to save changes?")
            if response is None:
                return False
            elif response is True:
                print("Should save")
            else:
                pass

        return True

    # Get the object of the current tab.
    @property
    def current_tab(self):
        return self.nb._nametowidget(self.nb.select())
        
