# QDNote

## Purpose

Lightweight text editor; effectively my own TKinter version of notepad++

## Where I found things

 * [General editor code](https://github.com/dh7qc/Python-Text-Editor)
 * [Line number edit window](https://github.com/rpcope1/CodePad)
 * [Status bar](https://github.com/israel-dryer/Notepad-Tk)

## Things TODO

 * Syntax highlighting
 * Search (ask string?)
 * Goto line (ask integer)

## Notes
 * I tried to use grid, but that is very twitchy, so I used pack