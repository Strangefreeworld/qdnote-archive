import tkinter as tk
import ttkbootstrap as ttk


class QDNoteStatusBar(ttk.Frame):
    def __init__(self, parent):
        super().__init__(parent, height=35, borderwidth=0)
        self.parent = parent

#        self.frame = ttk.Frame(self, height=35, borderwidth=0)
        self.line_numbers = tk.StringVar(value="Current Position")
        self.modified = tk.StringVar(value="Modified(?)")
        self.line_ending = tk.StringVar(value="The line ends")
        self.number_text = ttk.Label(self,
                                     borderwidth=2,
                                     relief="sunken",
                                     textvariable=self.line_numbers)
        self.modified_status = ttk.Label(self,
                                         borderwidth=2,
                                         relief="sunken",
                                         textvariable=self.modified)
        self.ending_status = ttk.Label(self,
                                       borderwidth=2,
                                       relief="sunken",
                                       textvariable=self.line_ending)
        self.grid(column=0, row=0, columnspan=3)
        self.number_text.grid(column=0, row=0, sticky=tk.W)
        self.modified_status.grid(column=1, row=0, sticky="nsew")
        self.ending_status.grid(column=2, row=0, sticky=tk.E)
        self.rowconfigure(0, weight=1)

