"""Original code from https://github.com/israel-dryer/Notepad-Tk/blob/master/widgets/statusbar.py"""

import tkinter as tk
from tkinter import ttk


class StatusBar(tk.Frame):
    """Status bar that shows text widget statistics"""
    def __init__(self, master):
        super().__init__(master, relief=tk.SUNKEN, bd=1)
        self.master = master
        ttk.Separator(self).pack(fill=tk.X, expand=tk.YES)

        self._position_var = tk.StringVar(value="Line:  1  Column:  0")
        tk.Label(self,
                 textvariable=self._position_var,
                 relief="sunken",
                 anchor=tk.W).pack(side=tk.LEFT)

        self._character_count = tk.StringVar(value="Chars:  0")
        tk.Label(self,
                 textvariable=self._character_count,
                 relief="sunken",
                 anchor=tk.W).pack(side=tk.LEFT)

        self._modified_var = tk.StringVar(value="  ")
        tk.Label(self,
                 textvariable=self._modified_var,
                 relief="sunken",
                 width=80,
                 anchor=tk.E).pack(side=tk.RIGHT)

        # Line Index
        # self.line_var = tk.StringVar()
        # self.line_var.set('1')
        # tk.Label(self, text='Line:', anchor=tk.W).pack(side=tk.LEFT)
        # tk.Label(self, textvariable=self.line_var, anchor=tk.W).pack(side=tk.LEFT)

        # Column Index
        # self.col_var = tk.StringVar()
        # self.col_var.set('0')
        # tk.Label(self, text='Col:', anchor=tk.W).pack(side=tk.LEFT)
        # tk.Label(self, textvariable=self.col_var, anchor=tk.W).pack(side=tk.LEFT)

        # Character Count
        #self.char_var = tk.StringVar()
        #self.char_var.set('0')
        #tk.Label(self, text='Chars:', anchor=tk.W).pack(side=tk.LEFT)
        #tk.Label(self, textvariable=self.char_var, anchor=tk.W).pack(side=tk.LEFT)

        # Word Count
        #self.word_var = tk.StringVar()
        #self.word_var.set('0')
        #tk.Label(self, text='Words:', anchor=tk.W).pack(side=tk.LEFT)
        #tk.Label(self, textvariable=self.word_var, anchor=tk.W).pack(side=tk.LEFT)

        # Event binding
        self.pack(side=tk.BOTTOM, fill=tk.X, padx=2, pady=2)

    def update_status(self, text_frame):
        """Update Status Bar"""
        line, col = text_frame.text_window.index(tk.INSERT).split('.')
        self._position_var.set(f"Line:  {line}  Column:  {col}")
        #self.line_var.set(line)
        #self.col_var.set(col)

        raw_text = text_frame.text_window.get('1.0', tk.END)
        spaces = raw_text.count(' ')

        char_count = "{:,d}".format(len(raw_text)-spaces)
        # self.char_var.set(char_count)
        self._character_count.set(f"Chars:  {char_count}")

        self._modified_var.set("Modified" if text_frame.modified else "  ")

        # word_list = [char for char in raw_text.split(' ') if char not in (' ', '', '\n')]
        # word_count = "{:,d}".format(len(word_list))
        # self.word_var.set(word_count)
