import hashlib
import tkinter as tk
import ttkbootstrap as ttk
import os
from line_numbers import TextLineNumbers
from custom_text import CustomText


class EditDocument(ttk.Frame):
    def __init__(self, parent, file_dir=None):
        ttk.Frame.__init__(self)
        self.parent = parent
        self._file = file_dir
        self._title = 'Untitled' if not file_dir else os.path.basename(file_dir)
        self._textbox = CustomText(self, relief='sunken', undo=True, borderwidth=0, wrap='none')
        self._textbox.tag_configure("bigfont", font=("Helvetica", "24", "bold"))
        self._line_numbers = TextLineNumbers(self, width=45, bg="#000000")
        self._line_numbers.attach(self._textbox)
        self._textbox.bind("<<Modified>>", self._text_modified)
        if file_dir:
            self.open_file(file_dir)
            self._line_numbers.redraw()
        self._line_numbers.pack(side="left", fill="y")
        self._textbox.pack(side="right", fill="both", expand=True)
        text_hash = self._hash_text()
        self._hash = text_hash
        self._textbox.bind("<<Change>>", self._text_modified)

    def _text_modified(self, _):
        self._line_numbers.redraw()

    def _hash_text(self):
        hasher = hashlib.new("sha1")
        hasher.update(self._textbox.get("1.0", "end").encode("utf-8"))
        return hasher.hexdigest()

    def open_file(self, filepath):
        with open(filepath, encoding="utf-8") as file_handle:
            self._textbox.delete(1.0, tk.END)
            self._textbox.insert(tk.END, file_handle.read())
            self._file = filepath
            self._title = os.path.basename(filepath)
            self._hash = self._hash_text()
            # self._line_numbers.redraw()

    def save_file(self):
        if not self._file:
            raise FileNotFoundError
        with open(self._file, "w") as file_handle:
            file_handle.write(self._textbox.get("1.0", tk.END))
            self._hash = self._hash_text()
            self._title = os.path.basename(self._file)

    def _total_lines(self) -> int:
        return int(self._textbox.index('end-1c').split('.')[0])

    def goto_line(self, line):
        line_count = self._total_lines()
        if line > line_count:
            print("Line number too large")
        else:
            self._textbox.mark_set("insert", f"{line}.0")
            self._textbox.see("insert")

    def save_file_as(self, filename):
        self._file = filename
        self.save_file()

    def undo(self):
        print("Editor undo")
        self._textbox.edit_undo()

    def select_all(self):
        self._textbox.tag_add(tk.SEL, "1.0", tk.END)
        self._textbox.mark_set(tk.INSERT, "1.0")
        self._textbox.see(tk.INSERT)
        return 'break'

    @property
    def filename(self):
        return self._file

    @property
    def title(self):
        return self._title

    @property
    def modified(self):
        current_hash = self._hash_text()
        return current_hash != self._hash

    @property
    def text_window(self):
        return self._textbox
    